## Description
This project will gather data about World of Warcraft characters and their compositions for dungeon groups. I want to collect data about:
- Character information: This includes the character's name, level, race, class, specialization, talents.
- Dungeon information: This includes the dungeon's name, difficulty level, and affixes.
- Group composition: This includes the number of players in the group, their roles (tank, healer, damage dealer), and their classes (also ilevel of he characters).

The data will be used to analyze the following:
- The most common group compositions for different dungeons and difficulty levels.
- The results of the research will be help players find the best group compositions for their needs.



### Ruby on Rails template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/rails).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Developing with Gitpod

This template has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

If you open this project in Gitpod, you'll get all dependencies pre-installed and Rails server will open a web preview.
